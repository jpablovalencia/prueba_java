/**
 * Created by jpablo on 21/08/15.
 */

$(document).ready(function() {
    $('#StudentTableContainer').jtable({
        title : ' Patients ',

        actions : {
           listAction : 'Controller?action=list',
            createAction : 'Controller?action=create',
            updateAction : 'Controller?action=update',
            deleteAction : 'Controller?action=delete'
        },
        fields : {
            id : {
                title : 'Paciente Id',
                width : '20%',
                key : true,
                list : true,
                edit : false,
                create : false,
                type: 'hidden'
            },
            nombres : {
                title : 'Names',
                width : '20%',
                edit : true
            },
            apellidos : {
                title : 'Last Names',
                width : '20%',
                edit : true
            },
            tipoDocumento : {
                title : 'Document Type',
                width : '20%',
                edit : true,
                options:{
                    'Cedula Ciudadania':'Cedula Ciudadania',
                    'Tarjeta Identidad':'Tarjeta Identidad'
                }
            },
            documento : {
                title : 'ID',
                width : '25%'

            },
            telefono : {
                title : 'Phone',
                width : '25%'

            },
            edad : {
                title : 'Age',
                width : '20%',
                edit : true
            },
            origen : {
                title : 'Origin',
                width : '20%',
                edit : true
            }
        }
    });
    $('#StudentTableContainer').jtable('load');
});