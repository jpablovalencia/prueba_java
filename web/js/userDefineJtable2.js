/**
 * Created by jpablo on 25/08/15.
 */
$(document).ready(function() {
    $('#StudentTableContainer2').jtable({
        title : ' Patient List ',
        actions : {
            listAction : 'Controller?action=list',
            //createAction : 'Controller?action=create',
            //updateAction : 'Controller?action=update',
            //deleteAction : 'Controller?action=delete'
        },
        fields : {
            id : {
                title : 'Paciente Id',
                width : '20%',
                key : true,
                list : true,
                edit : false,
                create : false
            },
            nombres : {
                title : 'Names',
                width : '20%',
                edit : false
            },
            apellidos : {
                title : 'LastNames',
                width : '20%',
                edit : false
            },
            tipoDocumento : {
                title : 'Document Type',
                width : '20%',
                edit : false
            },
            documento : {
                title : 'ID',
                width : '20%',
                edit : false
            },
            telefono : {
                title : 'Phone',
                width : '20%',
                edit : false
            },
            edad : {
                title : 'Age',
                width : '20%',
                edit : false
            },
            origen : {
                title : 'Origin',
                width : '20%',
                edit : false
            }
        }
    });
    $('#StudentTableContainer2').jtable('load');
});